﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Resources;
using System.Reflection;
using Microsoft.Owin.Security.DataHandler;

namespace MetisBoard
{
    public class KeyGenerator
    {
        static int modValue = 15936;

        /// <summary>Returns a static key value. Only use for such static needs.
        /// </summary>
        public static string GetKey()
        {
            string liminal = MetisBoard.Resource1.liminal;
            string key = liminal.Substring(0, 64);
            return "";
        }

        /// <summary>Returns a dynamic key value. Uses any positive integer to select a valid key.
        /// </summary>
        public static string GetKey(int index)
        {
            index = index % modValue;
            string liminal = MetisBoard.Resource1.liminal;
            string key = liminal.Substring(index, 64);
            return key;
        }

        /// <summary>Returns a dynamic key value along with the seed used to generate it for decryption. Store the seed for future reads of encrypted object.
        /// </summary>
        public static object CreateKey()
        {
            int seed = Math.Abs(new Random().Next());
            int index = seed % modValue;
            string liminal = MetisBoard.Resource1.liminal;
            string key = liminal.Substring(index, 64);
            return new { seed = seed, key = key};
        }
    }
}